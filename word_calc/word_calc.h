#ifndef WORD_CALC_H
#define WORD_CALC_H


//#include <string>
#include <QString>
class Word_calc
{
        double operands[2]={0.0,0.0};
        double operands_signs[2]={1.0,1.0};
        int operation = 0;
        double result = 0.0;

        enum oper {division, multiplication, addition, subtraction};
        void Add ();
        void Subtract ();
        void Multiplicate ();
        void Divide ();
        //bool Parse_input(std::string s);
        bool Parse_input(QString s);
    public:
        //void Calculate (std::string s);
        void Calculate (QString s);
        void Refresh_calculator ();
        Word_calc(){};
};

#endif // WORD_CALC_H
