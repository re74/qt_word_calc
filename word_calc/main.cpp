#include <QCoreApplication>
#include "word_calc.h"
/*
*@desc The program is aimed to calculate mathematical expression
    that consists of two operands and one operation.
    The expression is a word sequence.
    Available operations are "over", "times", "minus", "plus".
    The maximum available operand is "nine hundred ninty-nine".
    The minimum available operand is "minus nine hundred ninety-nine".
    The "hundred" has to always have a digit in front of it,
    e.g. "one hundred", "seven hundred".
    The -ty words has to be connected to following digits (if there are any) with dash,
    e.g. "thirty-nine", "sixty-four".
    The input has to start with a letter and finish after the last letter.
    The input can not start with an operation, unless it is "minus".
    The input should build a common-sense expression.
*/
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Word_calc word_calculator;

    //std::string input="";
    QString input;
    while (true)
    {
        /* OLD WAY
        std::cout <<"Enter your expression or Q to exit: ";
        std::getline (std::cin, input);
        */
        //NEW WAY
        QTextStream qin(stdin);
        QTextStream qout(stdout);
        qout <<"Enter your expression or Q to exit: ";
        qout.flush();

        input = qin.readLine();

        if ( input == "Q" || input == "q")
            break;

        word_calculator.Calculate(input);

    }

    qInfo("Bye!");
    //std::cout <<"Bye!"<<std::endl;
    return a.exec();
}
